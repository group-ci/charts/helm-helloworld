Use gitlab container registry as helm chart repository.

Usage:
```
helm chart pull registry.gitlab.com/group-ci/charts/helm-helloworld:master
helm chart export registry.gitlab.com/group-ci/charts/helm-helloworld:master -d /tmp/

helm upgrade --install helloworld /tmp/helm-helloworld
```


Ref: https://medium.com/@msvechla/distributing-helm-charts-via-gitlab-container-registry-a87ed2893647
